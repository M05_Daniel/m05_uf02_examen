<?php
    require_once 'connexio.php';

    require(__DIR__.'/lib/HTTPClient.php');
    require(__DIR__.'/lib/JSONParser.php');
    # Primer instanciar la classe, amb la base URL on hi ha la nostre api
    
class Dades extends Connexio{
    

    //Zona Alumnes
    public function insertarAlumne($nom, $cognoms, $email){        
        $params = [
            'nom' => $nom,
            'cognoms' => $cognoms,
            'mail' => $email       
        ];
        $client=Connexio::connectar();        
        $resultat = $client->query('/api/v1/alumne', $params, 'POST'); 
        return $resultat;                
    }

    public function llistarAlumnes(){        
        $client=Connexio::connectar();
        $resultat = $client->query('/api/v1/alumnes');
        return $resultat['data'];      
    }
    
    public function seleccionarAlumne($id){        
        $client=Connexio::connectar();   
        $params = [
            'id' => $id                   
        ];
        $resultat = $client->query('/api/v1/alumne/'.$id); 
        return $resultat['data'];
    }

    public function actualitzarAlumne($id, $nom, $cognoms, $email){    
        $params = [
            'nom' => $nom,
            'cognoms' => $cognoms,
            'mail' => $email       
        ];
        $client=Connexio::connectar();        
        $result = $client->query('/api/v1/alumne/'.$id, $params, 'PUT');
         
    }

    public function borrarAlumne($id){
        $client=Connexio::connectar();        
        $result = $client->query('/api/v1/alumne/'.$id, [], 'DELETE');
        
    }


    //Zona Assignatures
    public function insertarAssignatura($nom, $professor){        
        $params = [
            'nom' => $nom,
            'professor' => $professor
        ];
        $client=Connexio::connectar();        
        $resultat = $client->query('/api/v1/assignatura', $params, 'POST'); 
        return $resultat;                
    }

    public function llistarAssignatures(){        
        $client=Connexio::connectar();
        $resultat = $client->query('/api/v1/assignatures');
        return $resultat['data'];      
    }
    
    public function seleccionarAssignatura($id){ 
        $params=['id'=> $id];
        $client=Connexio::connectar();        
        $resultat = $client->query('/api/v1/assignatura/'.$id); 
        return $resultat['data'];
    }

    public function actualitzarAssignatura($id, $nom, $professor){    
        $params = [
            'id' => $id,
            'nom' => $nom,
            'cognoms' => $professor 
        ];
        $client=Connexio::connectar();        
        $result = $client->query('/api/v1/assignatura/:id', $params, 'PUT');
        return $resultat;  
    }

    public function borrarAssignatura($id){        
    }
}
?>