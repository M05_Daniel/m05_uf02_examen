<?php
/*function __autoload($class_name){
    require_once $class_name.'.php';
}
*/
require(__DIR__.'/lib/HTTPClient.php');
require(__DIR__.'/lib/JSONParser.php');

# Primer instanciar la classe, amb la base URL on hi ha la nostre api
$base_url   = 'http://127.0.0.1:3000';
$token      = 'C0UsWlYxXrMx81TKN2Eq';
$client     = new HTTPClient($base_url, $token);
var_dump($client);
# Si necessitem enviar paramatres a l'API, primer crearem un array amb els values
$llistatAlumnes = $client->query('/api/v1/alumnes');
var_dump($llistatAlumnes['data']);
/*foreach ($resultAlumnes['data'] as $key=> $alumne) {
    $params = [
        'nom' => $alumne->nom,
        'cognoms' => $alumne->cognoms,
        'mail' => $alumne->email,        
    ];
    
    $result = $client->query('/api/v1/alumne', $params, 'POST');
}
$resultAssignatures = JSONParser::parseFile(__DIR__.'/info/assignatures.json');

foreach ($resultAssignatures['data'] as $key => $assignatura) {
    
    $params = [
        'nom' => $assignatura->nom,
        'professor' => $assignatura->professor              
    ];
    
    $result = $client->query('/api/v1/assignatura', $params, 'POST');
}*/

## Mitjançant el client instanciat anteriorment, podem realitzar crides a l'API amb varis metodes
## i també enviar parametres.
# $client->query(string $uri, array $params = [], string $method='GET')
# Aquesta petició sempre ens retornarà un array amb dos camps:
## status =  true / false 
## data = retorn de l'API
/*
$result = $client->query('/api/v1/alumnes');
$result = $client->query('/api/v1/alumne', $params, 'POST');
$result = $client->query('/api/v1/alumne/:id', $params, 'PUT');
$result = $client->query('/api/v1/alumne/:id', [], 'DELETE');
*/
