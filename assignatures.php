<?php
  include 'header.php';
  require "crud.php";
  $dades = new Dades;

  //Variables Assignatura  
  if(isset($_GET['id'])){ $id=$_GET['id'];
  }else{ $id=""; }
  $assignatura="";
  $professor="";  
  $titol="Nova Assignatura";
  $btfun="Guardar";
  $btBorrar="hidden";

  //Comprovar Modificar i Eliminar
  if(isset($_GET['id'])){ 
    $titol="Modificació/Eliminació assignatura";
    $btfun="Modificar";
    $btBorrar="submit";
    
    $assignatura=$dades->seleccionarAssignatura($id);

    $nom = $assignatura->nom;    
    $cognoms=$assignatura->professor;
  }

  //Afegir, modificar o eliminar assignatura
  if (isset($_POST["Guardar"])) { //Inserir
   
    $nom=$_POST["nom"];
    $professor=$_POST["professor"];    
    $resposta= $dades->insertarAssignatura($nom,$professor);

  }elseif (isset($_POST["Modificar"])) { //Actualitzar

    $id=$_POST['id'];
    $nom=$_POST["nom"];
    $professor=$_POST["cognoms"];
    $resposta= $dades->actualitzarAssignatura($id,$nom,$professor);

  }elseif(isset($_POST["Eliminar"])){ //Eliminar

    $id=$_POST['id'];   
    $resposta= $dades->borrarAssignatura($id);

  }; 
?>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
  <form method="post">
  <div class="container">
  <h2>Nova assignatura</h2>
  
  <div class="form-inline">
    <label for="nom">Assignatura</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $id;?>">
    <input type="text" class="form-control" id="nom" placeholder="Entra assignatura" name="nom">
    <label for="professor">Professor</label>
    <input type="text" class="form-control" id="professor" placeholder="Entra professor" name="professor">        
    <input type="submit" name="<?php echo $btfun;?>" class="btn btn-primary" value="<?php echo $btfun;?>">
    <input type="<?php echo $btBorrar;?>" name="Eliminar" class="btn btn-primary" value="Eliminar">
  </div>
</div>
<br/>
<h1> Llista d'assignatures </h1>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">NOM</th>
          <th scope="col">PROFESSOR</th>
          <th scope="col">EDITAR/BORRAR</th>
        </tr>
      </thead>
      <tbody>
<?php
   //Carregar llistat d'assignatures 
  $llistaAssignatures = $dades->llistarAssignatures();

    foreach ($llistaAssignatures as $key => $assignatura){
        echo '<tr>
                <td>' . $assignatura->id . '</td>
                <td>' . $assignatura->nom . '</td>
                <td>' . $assignatura->professor . '</td>
                <td><a href="assignatures.php?id=' . $assignatura->id . '"><button>Editar/Borrar</button></a></td>
              </tr>';
    };
?>
      <tbody>
    </table> 
    <br/>
    <br/>  
  </form>
</body>
</html>

