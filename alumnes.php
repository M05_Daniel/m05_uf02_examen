<?php
  include 'header.php';
  require "crud.php";
  $dades = new Dades;

  //Variables Alumne  
  if(isset($_GET['id'])){ $id=$_GET['id'];
  }else{ $id=""; }
  $nom="";
  $cognoms="";
  $email="";
  $titol="Nou alumne";
  $btfun="Guardar";
  $btBorrar="hidden";

  //Comprovar Modificar i Eliminar
  if(isset($_GET['id'])){ 
    $titol="Modificació/Eliminació alumne";
    $btfun="Modificar";
    $btBorrar="submit";
    
    $alumne=$dades->seleccionarAlumne($id);

    $nom = $alumne->nom;    
    $cognoms=$alumne->cognoms;
    $email=$alumne->mail;      
  }

  //Afegir o modificar alumne
  if (isset($_POST["Guardar"])) { //Inserir
   
    $nom=$_POST["nom"];
    $cognoms=$_POST["cognoms"];
    $email=$_POST["email"];  
    $resposta= $dades->insertarAlumne($nom,$cognoms,$email);
    resetInfo();

  }elseif (isset($_POST["Modificar"])) { //Actualitzar

    $id=$_POST['id'];
    $nom=$_POST["nom"];
    $cognoms=$_POST["cognoms"];
    $email=$_POST["email"];
    $resposta= $dades->actualitzarAlumne($id,$nom,$cognoms,$email);
    resetInfo();
    
    
  }elseif(isset($_POST["Eliminar"])){ //Eliminar

    $id=$_POST['id'];   
    $resposta= $dades->borrarAlumne($id);
    resetInfo();

  };  
  function resetInfo(){
    $titol="Nou alumne";
    $btfun="Guardar";
    $btBorrar="hidden";
    $id="";
    $nom = "";    
    $cognoms="";
    $email="";
  }
?> 
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
  <form method="post">
  <div class="container">
  <h2><?php echo $titol;?></h2>
  
  <div class="form-inline">
    <label for="nom">Nom</label>
    <input type="hidden" class="form-control" id="id" placeholder="Entra nom" name="id" value="<?php echo $id;?>">
    <input type="text" class="form-control" id="nom" placeholder="Entra nom" name="nom" value="<?php echo $nom;?>">
    <label for="cognoms">Cognoms</label>
    <input type="text" class="form-control" id="cognoms" placeholder="Entra cognoms" name="cognoms" value="<?php echo $cognoms;?>">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo $email;?>">    
    <input type="submit" name="<?php echo $btfun;?>" class="btn btn-primary" value="<?php echo $btfun;?>">
    <input type="<?php echo $btBorrar;?>" name="Eliminar" class="btn btn-primary" value="Eliminar">
  </div>
</div>
</form>


<h1> Llista d'alumnes </h1>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">NOM</th>
          <th scope="col">COGNOMS</th>
          <th scope="col">EMAIL</th>
          <th scope="col">EDITAR/BORRAR</th>
        </tr>
      </thead>
      <tbody>
<?php
   //Carregar llistat d'alumnes 
  $llistaAlumnes = $dades->llistarAlumnes();

    foreach ($llistaAlumnes as $key => $alumne){
        echo '<tr>
                <td>' . $alumne->id . '</td>
                <td>' . $alumne->nom . '</td>
                <td>' . $alumne->cognoms . '</td>
                <td>' . $alumne->mail . '</td>
                <td><a href="alumnes.php?id=' . $alumne->id . '"><button>Editar/Borrar</button></a></td>
                
              </tr>';
    };
?>
      <tbody>
    </table> 
    <br/>
    <br/>  
 
</body>
</html>
